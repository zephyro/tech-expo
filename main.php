<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="layout">

            <?php include('inc/header.inc.php') ?>

            <div class="main-breadcrumb">
                <div class="section-wrap">
                    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="#" itemprop="item">Главная</a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <span itemprop="name">О компании</span>
                            <meta itemprop="position" content="2">
                        </li>
                    </ul>
                </div>
            </div>

            <section id="section_01" class="section section-light-gray">
                <div class="section-wrap">
                    <h1>Группа компаний «Техэкспо»</h1>

                    <div class="production">

                        <div class="production__nav">
                            <div class="production__nav_item"><a data-nav="1" class="production__nav_1 active" href="#">Дизельные электростанции</a></div>
                            <div class="production__nav_item"><a data-nav="2" class="production__nav_2" href="#">Энергокомплексы до 30 МВт</a></div>
                            <div class="production__nav_item"><a data-nav="3" class="production__nav_3" href="#">Контейнеры</a></div>
                            <div class="production__nav_item"><a data-nav="4" class="production__nav_4" href="#">Тендеры  44/223 ФЗ</a></div>
                            <div class="production__nav_item"><a data-nav="5" class="production__nav_5" href="#">Высоковольтное оборудование</a></div>
                            <div class="production__nav_item"><a data-nav="6" class="production__nav_6" href="#">Техобслуживание</a></div>
                            <div class="production__nav_item"><a data-nav="7" class="production__nav_7" href="#">Аренда</a></div>
                            <div class="production__nav_item"><a data-nav="8" class="production__nav_8" href="#">EPC-контракты</a></div>
                        </div>
                        <div  id="production_slider" class="point_offset"></div>
                        <div class="production__wrap">
                            <div class="production__slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Производим</span>
                                                    <strong>Дизельные электростанции</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Производим</span>
                                                    <strong>Энергокомплексы до 30 МВт</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Производим</span>
                                                    <strong>Контейнеры</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Производим</span>
                                                    <strong>Тендеры  44/223 ФЗ</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Производим</span>
                                                    <strong>Высоковольтное оборудовани</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Предлагаем</span>
                                                    <strong>Техобслуживание</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Предлагаем</span>
                                                    <strong>Аренду</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="production-item">
                                            <div class="production-item__image">
                                                <div class="production-item__image_wrap">
                                                    <img src="images/production__slider_image.png" class="img_fluid" alt="">
                                                </div>
                                            </div>
                                            <div class="production-item__content">
                                                <div class="production-item__title">
                                                    <span>Предлагаем</span>
                                                    <strong>EPC-контракты</strong>
                                                </div>
                                                <div class="production-item__text">Единичной мощностью от 40 до 2 500 кВт</div>
                                                <div class="production-item__button">
                                                    <a class="btn_main" href="#">
                                                        <span>Каталог электростанций</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            <div class="production__button production__button_prev">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                            <div class="production__button production__button_next">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 14"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <section class="file_box">
                <div class="section-wrap">
                    <div class="file_box__row">
                        <div class="file_box__item">
                            <a class="file_link" href="#">
                                <span>Досье поставщика</span>
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 49 49" fill="none"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                        <div class="file_box__item">
                            <a class="file_link" href="#">
                                <span>Брошюра о выполненных проектах</span>
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 49 49" fill="none"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="section_02" class="activity">
                <div class="section-wrap">

                    <div class="activity__header">
                        <div class="activity__header_title">Направления деятельности:</div>
                        <div class="activity__header_nav">
                            <button type="button" class="activity__nav activity__nav_prev">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                            <button type="button" class="activity__nav activity__nav_next">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                        </div>
                    </div>

                    <div class="activity__slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" class="activity__item">
                                    <div class="activity__item_image">
                                        <img src="images/activity__image_01.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="activity__item_title"><span>Дизельные электростанции</span></div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="activity__item">
                                    <div class="activity__item_image">
                                        <img src="images/activity__image_02.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="activity__item_title"><span>Атомная энергетика</span></div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="activity__item">
                                    <div class="activity__item_image">
                                        <img src="images/activity__image_03.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="activity__item_title"><span>Производство контейнеров</span></div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="activity__item">
                                    <div class="activity__item_image">
                                        <img src="images/activity__image_01.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="activity__item_title"><span>Дизельные электростанции</span></div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="activity__item">
                                    <div class="activity__item_image">
                                        <img src="images/activity__image_02.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="activity__item_title"><span>Атомная энергетика</span></div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" class="activity__item">
                                    <div class="activity__item_image">
                                        <img src="images/activity__image_03.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="activity__item_title"><span>Производство контейнеров</span></div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>

                </div>
            </section>

            <section id="section_03" class="warranty">
                <div class="section-wrap">
                    <div class="warranty__header">Качество нашей продукции и гарантии</div>
                    <div class="warranty__item">
                        <div class="warranty__item_icon">
                            <img src="img/warranty__icon_01.png" class="img_fluid" alt="">
                        </div>
                        <div class="warranty__item_text">Предоставляем расширенную гарантию 5 лет на выпускаемую продукцию</div>
                    </div>
                    <div class="warranty__item">
                        <div class="warranty__item_icon">
                            <img src="img/warranty__icon_02.png" class="img_fluid" alt="">
                        </div>
                        <div class="warranty__item_text">Применяем собственную лицензированную электролабораторию №14-53/ЭЛ-19</div>
                    </div>
                    <div class="warranty__item">
                        <div class="warranty__item_icon">
                            <img src="img/warranty__icon_03.png" class="img_fluid" alt="">
                        </div>
                        <div class="warranty__item_text">Состоим в СРО проектировщиков (СРО-П-161-09092010) и в СРО строителей (СРО-С-258-11012013)</div>
                    </div>
                    <div class="warranty__item">
                        <div class="warranty__item_icon">
                            <img src="img/warranty__icon_04.png" class="img_fluid" alt="">
                        </div>
                        <div class="warranty__item_text">Применяем систему менеджмента в области охраны труда (OHSAS 18000:2007)</div>
                    </div>
                    <div class="warranty__item">
                        <div class="warranty__item_icon">
                            <img src="img/warranty__icon_05.png" class="img_fluid" alt="">
                        </div>
                        <div class="warranty__item_text">Состоим в Санкт-Петербургской торгово-промышленной палате</div>
                    </div>
                </div>
            </section>

            <section id="section_04" class="manufacture">
                <div class="section-wrap">
                    <div class="manufacture__header">Наше производство</div>

                    <div class="manufacture__mobile">
                        <div class="manufacture__slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/manufacture__image_01.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_01.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>15 лет производственному <br/>комплексу</span></div>
                                    </a>
                                    <a href="images/manufacture__image_02.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_02.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Проектно-конструкторское <br/>бюро</span></div>
                                    </a>
                                    <a href="images/manufacture__image_03.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_03.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Покрасочный <br/>цех</span></div>
                                    </a>
                                    <a href="images/manufacture__image_04.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_04.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Площадь 2000 м2</span></div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/manufacture__image_05.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_05.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Современные <br/>станки</span></div>
                                    </a>
                                    <a href="images/manufacture__image_06.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_06.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Резка по металлу</span></div>
                                    </a>
                                    <a href="images/manufacture__image_07.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_07.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Отдел технического <br/>контроля</span></div>
                                    </a>
                                    <a href="images/manufacture__image_08.jpg" class="manufacture__item" data-fancybox="manufacture">
                                        <div class="manufacture__item__image">
                                            <img src="images/manufacture__image_08.jpg" class="img_fluid" alt="">
                                        </div>
                                        <div class="manufacture__item_title"><span>Цех сборки <br/>электроизделий</span></div>
                                    </a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                    <div class="manufacture__desktop">

                        <div class="manufacture__row manufacture_gallery">

                            <a href="images/manufacture__image_01.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_01.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>15 лет производственному <br/>комплексу</span></div>
                            </a>
                            <a href="images/manufacture__image_02.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_02.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Проектно-конструкторское <br/>бюро</span></div>
                            </a>
                            <a href="images/manufacture__image_03.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_03.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Покрасочный <br/>цех</span></div>
                            </a>
                            <a href="images/manufacture__image_04.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_04.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Площадь 2000 м2</span></div>
                            </a>
                            <a href="images/manufacture__image_05.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_05.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Современные <br/>станки</span></div>
                            </a>
                            <a href="images/manufacture__image_06.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_06.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Резка по металлу</span></div>
                            </a>
                            <a href="images/manufacture__image_07.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_07.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Отдел технического <br/>контроля</span></div>
                            </a>
                            <a href="images/manufacture__image_08.jpg" class="manufacture__item" data-fancybox="manufacture">
                                <div class="manufacture__item__image">
                                    <img src="images/manufacture__image_08.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="manufacture__item_title"><span>Цех сборки <br/>электроизделий</span></div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>

            <section id="section_05" class="engineering">
                <div class="section-wrap">
                    <div class="engineering__header">Инжиниринговый центр «Техэкспо»</div>
                    <div class="engineering__row">
                        <div class="engineering__info">
                            <div class="engineering__title">Проектируем:</div>
                            <div class="engineering__nav">
                                <a href="#" class="engineering__nav_1 active" data-slide="1">
                                    <span>Дизель-генераторные установки</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="engineering__nav_2" data-slide="2">
                                    <span>Энергокомплексы из нескольких ДГУ</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="engineering__nav_3" data-slide="3">
                                    <span>Контейнеры с электрооборудованием</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="engineering__nav_4" data-slide="4">
                                    <span>РУНН, ВРУ, ГРЩ, АВР</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="engineering__nav_5" data-slide="5">
                                    <span>Высоковольтные электростанции 6,3/10,5 кВ</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                            </div>
                        </div>
                        <div class="engineering__media">
                            <div class="engineering__slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img src="images/engineering__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="images/engineering__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="images/engineering__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="images/engineering__image.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="images/engineering__image.jpg" class="img_fluid" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="engineering__control">
                                <div class="engineering__pagination swiper-pagination"></div>
                                <div class="engineering__button engineering__button_prev">
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                                <div class="engineering__button engineering__button_next">
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="section_06" class="list">
                <div class="section-wrap">
                    <div class="list__header">Преимущества:</div>
                    <ul class="list__content">
                        <li><span>Имеем 4 штатных инженеров с опытом проектирование систем энергоснабжения более 15 лет</span></li>
                        <li><span>Создаем чертежи и 3D модели продукции по требованиям ЕСКД, ЕСТП и ГОСТ</span></li>
                        <li><span>Создаем схемы драйкулеров, топливной системы, АВР, ЩСН, РУНН со спецификацими на комплектущие</span></li>
                        <li><span>Составляем сервисные инструкции по работе и техническому обслуживанию энергокомплексов</span></li>
                        <li><span>Разрабатываем технические условия на ДГУ и высоковольтное оборудование</span></li>
                        <li><span>Создаем сметы, проектную и рабочую документацию, в том числе на строительно-монтажные работы</span></li>
                    </ul>
                </div>
            </section>

            <section class="brands">
                <div class="section-wrap">
                    <div class="brands__header">Инжиниринговый центр «Техэкспо»</div>
                    <div class="brands__row">
                        <div class="brands__info">
                            <div class="brands__nav">
                                <a href="#" class="brands__nav_1 active" data-slide="1">
                                    <span>FPT (Iveco)</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_2" data-slide="2">
                                    <span>MTU</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_3" data-slide="3">
                                    <span>Mitsubishi</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_4" data-slide="4">
                                    <span>Cummins</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_5" data-slide="5">
                                    <span>Perkins</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_6" data-slide="6">
                                    <span>Scania</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_7" data-slide="7">
                                    <span>Doosan</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#" class="brands__nav_8" data-slide="8">
                                    <span>Volvo</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__chevron_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                            </div>
                        </div>
                        <div class="brands__media">
                            <div class="brands__slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brands__item">
                                            <img src="images/brands__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brands__control">
                                <div class="brands__pagination swiper-pagination"></div>
                                <div class="brands__button brands__button_prev">
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                                <div class="brands__button brands__button_next">
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="section_07" class="list">
                <div class="section-wrap">
                    <div class="list__header">Наши услуги:</div>
                    <ul class="list__content">
                        <li><span>Проводим бесплатные предмонтажные осмотры по всей территории РФ и СНГ</span></li>
                        <li><span>Оказываем сопровождение в тендерах, защищаем проекты,составляем уникальные ТЗ</span></li>
                        <li><span>Разрабатываем проектную документацию, генеральные планы, рабочие чертежи</span></li>
                        <li><span>Составляем сметную документацию и технико-экономические обоснования проектов</span></li>
                        <li><span>Выполняем авторский надзор проектов своими силами</span></li>
                        <li><span>Организуем приемо-сдаточные испытания</span></li>
                        <li><span>Готовим исполнительную документацию</span></li>
                        <li><span>Получаем разрешения на допуск в эксплуатацию энергоустановок в Ростехнадзоре</span></li>
                    </ul>
                </div>
            </section>

            <section id="section_08" class="docs docs_dark">
                <div class="section-wrap">
                    <div class="docs__header">Лицензии компании</div>
                    <div class="docs__wrap">
                        <div class="license-slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/doc__image_01.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_01.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Свидетельство на товарный знак №705643</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_02.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_02.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Выписка из реестра членов СРО, осуществляющих подготовку проектной документации</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_03.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_03.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Выписка из реестра членов СРО в сфере строительства</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_04.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_04.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Свидетельство о членстве в СПБ торгово-промышленной палате №33-5041</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="swiper-slide">
                                    <a href="images/doc__image_01.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_01.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Свидетельство на товарный знак №705643</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_02.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_02.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Выписка из реестра членов СРО, осуществляющих подготовку проектной документации</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_03.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_03.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Выписка из реестра членов СРО в сфере строительства</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_04.jpg" class="docs__item" target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_04.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 49 49"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Свидетельство о членстве в СПБ торгово-промышленной палате №33-5041</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="docs__nav">
                            <button class="docs__button docs__button_prev license-slider-prev">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                            <div class="docs__pagination swiper-pagination"></div>
                            <button class="docs__button docs__button_next license-slider-next">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </section>

            <section id="section_09" class="docs">
                <div class="section-wrap">
                    <div class="docs__header">Дилерские сертификаты</div>
                    <div class="docs__wrap">
                        <div class="cert-slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/doc__image_05.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_05.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Green Power <br/>(Италия)</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_06.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_06.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>CTM <br/>(Италия)</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_07.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_07.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>EMSA <br/>(Турция)</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_08.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_08.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>ООО «Комплексные решения» <br/>(Onis Visa, Италия)</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="swiper-slide">
                                    <a href="images/doc__image_05.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_05.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>Green Power <br/>(Италия)</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_06.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_06.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>CTM <br/>(Италия)</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_07.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_07.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>EMSA <br/>(Турция)</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/doc__image_08.jpg" class="docs__item"  target="_blank">
                                        <div class="docs__item_image">
                                            <img src="images/doc__image_08.jpg" class="img_fluid" alt="">
                                            <div class="docs__item_icon">
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 23 23"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf_red" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div class="docs__item_title">
                                            <span>ООО «Комплексные решения» <br/>(Onis Visa, Италия)</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="docs__nav">
                            <button class="docs__button docs__button_prev cert-slider-prev">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                            <div class="cert__pagination swiper-pagination"></div>
                            <button class="docs__button docs__button_next cert-slider-next">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </section>

            <section id="section_10" class="reference">
                <div class="section-wrap">

                    <div class="reference__header">
                        <div class="reference__header_title">Референс лист выполненных проектов по 44-ФЗ и 223-ФЗ</div>
                        <div class="reference__header_text">
                            За период c 2013 по 2020
                            год заключено более 200 государственных контрактов
                            на общую сумму 600 млн руб.
                        </div>
                    </div>

                    <div class="reference__wrap">
                        <div class="reference__content">
                            <div class="reference__content_wrap">
                                <table class="main-table">
                                    <tr>
                                        <th class="text_nowrap">Номер контракта</th>
                                        <th class="text_nowrap"><span class="table-sort">Сумма</span></th>
                                        <th class="text_nowrap">ФЗ</th>
                                        <th class="text_nowrap">Предмет контракта</th>
                                        <th class="text_nowrap">Заказчик</th>
                                        <th class="text_nowrap"><span class="table-sort">Дата контракта</span></th>
                                    </tr>
                                    <tr>
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>
                                    <tr>
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>
                                    <tr>
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>

                                    <tr class="hide_part_table hide">
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>
                                    <tr class="hide_part_table hide">
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>
                                    <tr class="hide_part_table hide">
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>
                                    <tr class="hide_part_table hide">
                                        <td class="text_nowrap">1781502228816000711	</td>
                                        <td class="text_nowrap"><strong>61 149 442 руб.</strong></td>
                                        <td class="text_nowrap">44-ФЗ</td>
                                        <td>Реконструкция котельной и инженерных сетей санатория «Плес», г. Плес, Ивановская область филиала «Санаторий «Плес» ФГУП «Санкт-Петербургский научно-исследовательский институт фтизиопульмонологии»</td>
                                        <td>ФГБУ "СПБ НИИФ" МИНЗДРАВА РОССИИ</td>
                                        <td class="text_nowrap">10.08.2015</td>
                                    </tr>
                                </table>
                                <div class="reference__nav">
                                    <button type="button" class="reference__view reference_toggle" data-base="Показать еще выполненные проекты" data-view="Скрыть проекты">
                                        <span>Показать еще выполненные проекты</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </button>
                                    <a href="#" class="reference__download">Скачать полный референс</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="reference__mobile">
                        <button type="button" class="reference__view">
                            <span>Показать еще выполненные проекты</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </button>
                    </div>

                </div>
            </section>

            <section id="section_11" class="person">
                <div class="section-wrap">
                    <div class="person__header">Менеджмент компании</div>
                    <div class="person__row">

                        <div class="person__col person__col_xs">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__01.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Филипп Врацких</div>
                                    <div class="person__item_staff">Генеральный директор</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79219335454" class="person__item_phone">+7 921 933-54-54</a></li>
                                        <li><a href="mailto:f.vratskikh@tech- expo.ru" class="person__item_email">f.vratskikh@tech- expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_xs">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__02.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Илья Козлов</div>
                                    <div class="person__item_staff">Коммерческий директор</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79315996170" class="person__item_phone">+7 931 599-61-70</a></li>
                                        <li><a href="mailto:ik@tech-expo.ru" class="person__item_email">ik@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_xs">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__03.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Руслан Касаев</div>
                                    <div class="person__item_staff">Директор по развитию</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79857691695" class="person__item_phone">+7 985 769-16-95</a></li>
                                        <li><a href="mailto:rk@tech-expo.ru" class="person__item_email">rk@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_sm">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__04.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Игорь Ишин</div>
                                    <div class="person__item_staff">Технический директор</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218460896" class="person__item_phone">+7 921 846-08-96</a></li>
                                        <li><a href="mailto:ii@tech-expo.ru" class="person__item_email">ii@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_sm">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__05.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Антон Александров</div>
                                    <div class="person__item_staff">Директор по рекламе</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:88005508394" class="person__item_phone">8 800 550-83-94</a></li>
                                        <li><a href="mailto:info@tech-expo.ru" class="person__item_email">info@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_sm">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__06.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Руслан Касаев</div>
                                    <div class="person__item_staff">Заместитель директора по реализации проектов</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218461322" class="person__item_phone">+7 921 846-13-22</a></li>
                                        <li><a href="mailto:kn@tech-expo.ru" class="person__item_email">kn@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__07.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Филипп Крекотень</div>
                                    <div class="person__item_staff">Заместитель директора по сервису/ЗИП/аренде</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79217645161" class="person__item_phone">+7 921 764-51-61</a></li>
                                        <li><a href="mailto:" class="person__item_email"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__08.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Дарина Максимова</div>
                                    <div class="person__item_staff">Заместитель директора по тендерам и аукционам</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79315996168" class="person__item_phone">+7 931-599-61-68</a></li>
                                        <li><a href="mailto:d.maksimova@tech-expo.ru" class="person__item_email">d.maksimova@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__09.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Галина Грицевич</div>
                                    <div class="person__item_staff">Заместитель директора по реализации проектов</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218461322" class="person__item_phone">+7 921 846-13-22</a></li>
                                        <li><a href="mailto:kn@tech-expo.ru" class="person__item_email">kn@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_hide">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__04.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Игорь Ишин</div>
                                    <div class="person__item_staff">Технический директор</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218460896" class="person__item_phone">+7 921 846-08-96</a></li>
                                        <li><a href="mailto:ii@tech-expo.ru" class="person__item_email">ii@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_hide">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__05.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Антон Александров</div>
                                    <div class="person__item_staff">Директор по рекламе</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:88005508394" class="person__item_phone">8 800 550-83-94</a></li>
                                        <li><a href="mailto:info@tech-expo.ru" class="person__item_email">info@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_hide">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__06.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Руслан Касаев</div>
                                    <div class="person__item_staff">Заместитель директора по реализации проектов</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218461322" class="person__item_phone">+7 921 846-13-22</a></li>
                                        <li><a href="mailto:kn@tech-expo.ru" class="person__item_email">kn@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_hide">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__08.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Дарина Максимова</div>
                                    <div class="person__item_staff">Заместитель директора по тендерам и аукционам</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79315996168" class="person__item_phone">+7 931-599-61-68</a></li>
                                        <li><a href="mailto:d.maksimova@tech-expo.ru" class="person__item_email">d.maksimova@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_hide">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__09.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Галина Грицевич</div>
                                    <div class="person__item_staff">Заместитель директора по реализации проектов</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218461322" class="person__item_phone">+7 921 846-13-22</a></li>
                                        <li><a href="mailto:kn@tech-expo.ru" class="person__item_email">kn@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="person__col person__col_hide">
                            <div class="person__item">
                                <div class="person__item_image">
                                    <img src="images/person__06.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="person__item_content">
                                    <div class="person__item_name">Руслан Касаев</div>
                                    <div class="person__item_staff">Заместитель директора по реализации проектов</div>
                                    <ul class="person__item_contact">
                                        <li><a href="tel:+79218461322" class="person__item_phone">+7 921 846-13-22</a></li>
                                        <li><a href="mailto:kn@tech-expo.ru" class="person__item_email">kn@tech-expo.ru</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="person__button">
                        <button type="button" class="person__view" data-view="Скрыть список" data-base="Показать список всех сотрудников">
                            <span>Показать список всех сотрудников</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </button>
                    </div>
                </div>
            </section>

            <section id="section_12" class="comments">
                <div class="section-wrap">

                    <div class="comments__header">
                        <div class="comments__header_title">СМИ о нашей компании</div>
                        <div class="comments__header_nav">
                            <button type="button" class="comments__nav comments__nav_prev">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                            <button type="button" class="comments__nav comments__nav_next">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 8 13"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </button>
                        </div>
                    </div>

                    <div class="comments__mobile">
                        <div class="comments__item">
                            <a href="#" class="comments__item_image">
                                <div class="comments__item_wrap">
                                    <img src="images/comments__logo_01.png" class="img_fluid" alt="">
                                </div>
                            </a>
                            <a class="comments__item_link" href="#">expert.ru</a>
                            <div class="comments__item_title">
                                Expert Online
                                (медиахолдинг
                                «Эксперт»)
                            </div>
                        </div>
                        <div class="comments__item">
                            <a href="#" class="comments__item_image">
                                <div class="comments__item_wrap">
                                    <img src="images/comments__logo_03.png" class="img_fluid" alt="">
                                </div>
                            </a>
                            <a class="comments__item_link" href="#">dp.ru</a>
                            <div class="comments__item_title">DP.RU <br/>(«Деловой Петербург»)</div>
                        </div>
                        <div class="comments__item">
                            <a href="#" class="comments__item_image">
                                <div class="comments__item_wrap">
                                    <img src="images/comments__logo_03.png" class="img_fluid" alt="">
                                </div>
                            </a>
                            <a class="comments__item_link" href="#">dp.ru</a>
                            <div class="comments__item_title">DP.RU <br/>(«Деловой Петербург»)</div>
                        </div>
                        <div class="comments__item">
                            <a href="#" class="comments__item_image">
                                <div class="comments__item_wrap">
                                    <img src="images/comments__logo_04.png" class="img_fluid" alt="">
                                </div>
                            </a>
                            <a class="comments__item_link" href="#">oblgazeta.ru</a>
                            <div class="comments__item_title">Областная газета (Екатеринбург)</div>
                        </div>
                        <div class="comments__item">
                            <a href="#" class="comments__item_image">
                                <div class="comments__item_wrap">
                                    <img src="images/comments__logo_05.png" class="img_fluid" alt="">
                                </div>
                            </a>
                            <a class="comments__item_link" href="#">newkaliningrad.ru</a>
                            <div class="comments__item_title">Новый Калининград</div>
                        </div>
                        <div class="comments__item">
                            <a href="#" class="comments__item_image">
                                <div class="comments__item_wrap">
                                    <img src="images/comments__logo_06.png" class="img_fluid" alt="">
                                </div>
                            </a>
                            <a class="comments__item_link" href="#">kgd.ru</a>
                            <div class="comments__item_title">Калининград.Ru</div>
                        </div>
                    </div>

                    <div class="comments__desktop">
                        <div class="comments__slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="comments__item">
                                        <a href="#" class="comments__item_image">
                                            <div class="comments__item_wrap">
                                                <img src="images/comments__logo_01.png" class="img_fluid" alt="">
                                            </div>
                                        </a>
                                        <a class="comments__item_link" href="#">expert.ru</a>
                                        <div class="comments__item_title">
                                            Expert Online
                                            (медиахолдинг
                                            «Эксперт»)
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="comments__item">
                                        <a href="#" class="comments__item_image">
                                            <div class="comments__item_wrap">
                                                <img src="images/comments__logo_02.png" class="img_fluid" alt="">
                                            </div>
                                        </a>
                                        <a class="comments__item_link" href="#">tayga.info</a>
                                        <div class="comments__item_title">Тайга.инфо <br/>(Новосибирск)	</div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="comments__item">
                                        <a href="#" class="comments__item_image">
                                            <div class="comments__item_wrap">
                                                <img src="images/comments__logo_03.png" class="img_fluid" alt="">
                                            </div>
                                        </a>
                                        <a class="comments__item_link" href="#">dp.ru</a>
                                        <div class="comments__item_title">DP.RU <br/>(«Деловой Петербург»)</div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="comments__item">
                                        <a href="#" class="comments__item_image">
                                            <div class="comments__item_wrap">
                                                <img src="images/comments__logo_04.png" class="img_fluid" alt="">
                                            </div>
                                        </a>
                                        <a class="comments__item_link" href="#">oblgazeta.ru</a>
                                        <div class="comments__item_title">Областная газета (Екатеринбург)</div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="comments__item">
                                        <a href="#" class="comments__item_image">
                                            <div class="comments__item_wrap">
                                                <img src="images/comments__logo_05.png" class="img_fluid" alt="">
                                            </div>
                                        </a>
                                        <a class="comments__item_link" href="#">newkaliningrad.ru</a>
                                        <div class="comments__item_title">Новый Калининград</div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="comments__item">
                                        <a href="#" class="comments__item_image">
                                            <div class="comments__item_wrap">
                                                <img src="images/comments__logo_06.png" class="img_fluid" alt="">
                                            </div>
                                        </a>
                                        <a class="comments__item_link" href="#">kgd.ru</a>
                                        <div class="comments__item_title">Калининград.Ru</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>

            <?php include('inc/selection.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

            <div class="page-nav">
                <ul id="sidenav">
                    <li class="current"><a href="#section_01"><i>1</i><span>Техэкспо</span></a></li>
                    <li><a href="#section_02"><i>2</i><span>Деятельность</span></a></li>
                    <li><a href="#section_03"><i>4</i><span>Качество и гарантии</span></a></li>
                    <li><a href="#section_04"><i>3</i><span>Производство</span></a></li>
                    <li><a href="#section_05"><i>5</i><span>Инжиниринговый центр</span></a></li>
                    <li><a href="#section_06"><i>6</i><span>Преимущества</span></a></li>
                    <li><a href="#section_07"><i>7</i><span>Услуги</span></a></li>
                    <li><a href="#section_08"><i>8</i><span>Лицензии</span></a></li>
                    <li><a href="#section_09"><i>9</i><span>Сертификаты</span></a></li>
                    <li><a href="#section_10"><i>10</i><span>Выполненные проекты</span></a></li>
                    <li><a href="#section_11"><i>11</i><span>Менеджмент</span></a></li>
                    <li><a href="#section_12"><i>12</i><span>СМИ о нас</span></a></li>
                </ul>
            </div>

        </div>

        <?php include('inc/modal.inc.php') ?>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
