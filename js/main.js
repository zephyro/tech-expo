
$( document ).ready(function() {

    // SVG IE11 support
    svg4everybody();

    $(".btn_modal").fancybox({
        'padding'    : 0
    });


    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $('.nav_toggle').toggleClass('nav_open');
        //  $('.nav-mobile').slideToggle('fast');
        $('.nav-mobile').toggleClass('nav_open');
    });


    $( window ).resize(function() {
        if ($(window).width() > 1023) {
            $('.nav-mobile').removeClass('nav_open');
            $('.nav_toggle').removeClass('nav_open');
        }
    });


    $('.city_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $('.city-switch').slideToggle('fast');
    })

    $('.search_toggle').on('click', function(e){
        e.preventDefault();

        $('.head__search_content').toggleClass('open');
    })


    // Hide dropdown

    $('body').click(function (event) {

        if ($(event.target).closest(".cities-wrapper").length === 0) {

            if ($(event.target).closest(".city_toggle").length === 0) {

                $('.city-switch').slideUp('fast');
            }
        }

    });

    $('#sidenav').onePageNav({
        filter: ':not(.external)'
    });

    var production = new Swiper ('.production__slider', {
        loop: true,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.production__button_next',
            prevEl: '.production__button_prev',
        }
    });

    production.on('slideChange', function () {
        var sl = '.production__nav_' + (production.realIndex + 1);
        $('.production__nav').find('a').removeClass('active');
        $('.production__nav').find(sl).addClass('active');
    });

    $('.production__nav a').on('click', function(e) {
        e.preventDefault();
        var slide = $(this).attr("data-nav");
        $('.production__nav').find('a').removeClass('active');
        $(this).addClass('active');
        if ($(window).width() < 1200) {
            $('html,body').stop().animate({ scrollTop: $('#production_slider').offset().top }, 600);
        }
        production.slideTo(slide, 600);
    });

    var activity = new Swiper('.activity__slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 12,
        navigation: {
            nextEl: '.activity__nav_next',
            prevEl: '.activity__nav_prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            370: {
                slidesPerView: 2,
                spaceBetween: 12,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 26,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
        }
    });


    var manufacture = new Swiper ('.manufacture__slider', {
        loop: true,
        spaceBetween: 20,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var engineering = new Swiper ('.engineering__slider', {
        loop: true,
        spaceBetween: 20,
        navigation: {
            nextEl: '.engineering__button_next',
            prevEl: '.engineering__button_prev',
        },
        pagination: {
            el: '.engineering__pagination',
            clickable: true,
        },
        breakpoints: {
            1200: {
                direction: 'vertical'
            },
        }
    });

    engineering.on('slideChange', function () {
        var sl = '.engineering__nav_' + (engineering.realIndex + 1);
        $('.engineering__nav').find('a').removeClass('active');
        $('.engineering__nav').find(sl).addClass('active');
    });

    $('.engineering__nav a').on('click', function(e) {
        e.preventDefault();
        var slide = $(this).attr("data-slide");
        $('.engineering__nav').find('a').removeClass('active');
        $(this).addClass('active');
        engineering.slideTo(slide, 600);
    });



    var brands = new Swiper ('.brands__slider', {
        loop: true,
        spaceBetween: 20,
        navigation: {
            nextEl: '.brands__button_next',
            prevEl: '.brands__button_prev',
        },
        pagination: {
            el: '.brands__pagination',
            clickable: true,
        },
        breakpoints: {
            1200: {
                direction: 'vertical'
            },
        }
    });

    brands.on('slideChange', function () {
        var sl = '.brands__nav_' + (brands.realIndex + 1);
        $('.brands__nav').find('a').removeClass('active');
        $('.brands__nav').find(sl).addClass('active');
    });

    $('.brands__nav a').on('click', function(e) {
        e.preventDefault();
        var slide = $(this).attr("data-slide");
        $('.brands__nav').find('a').removeClass('active');
        $(this).addClass('active');
        brands.slideTo(slide, 600);
    });



    var license = new Swiper('.license-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.license-slider-next',
            prevEl: '.license-slider-prev',
        },
        pagination: {
            el: '.docs__pagination',
            clickable: true,
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
            },
            1024: {
                slidesPerView: 4,
            },
            1200: {
                slidesPerView: 4,
            },
        }
    });

    var cert = new Swiper('.cert-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.cert-slider-next',
            prevEl: '.cert-slider-prev',
        },
        pagination: {
            el: '.cert__pagination',
            clickable: true,
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
            },
            1024: {
                slidesPerView: 4,
            },
            1200: {
                slidesPerView: 4,
            },
        }
    });


    var comments = new Swiper('.comments__slider', {
        loop: true,
        slidesPerView: 5,
        spaceBetween: 0,
        navigation: {
            nextEl: '.comments__nav_next',
            prevEl: '.comments__nav_prev',
        },
        breakpoints: {
            1200: {
                slidesPerView: 6,
            },
        }
    });

    var completed = new Swiper('.completed__slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    });

    // Tabs

    $('.selection__nav a').on('click', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.selection');

        $(this).closest('.selection__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.selection__tab').removeClass('active');
        box.find(tab).addClass('active');
    });


    $('.filter__nav a').on('click', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.filter');

        $(this).closest('.filter__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.filter__tab').removeClass('active');
        box.find(tab).addClass('active');
    });


    $('.goods__comparison_add').on('click ', function(e){
        e.preventDefault();
        $(this).closest('.goods__comparison').addClass('checked');
    });

    $('.sort').on('click touchstart', function(e){
        e.preventDefault();
        $(this).toggleClass('invert');
    });

    var comparison = new Swiper('.comparison__slider', {
        init: false,
        freeMode: true,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.comparison__nav_next',
            prevEl: '.comparison__nav_prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 'auto',
            },
            1200: {
                slidesPerView: 3,
            },
        }
    });

    $('.btn_comparison').on('click', function(e){
        e.preventDefault();
        $.fancybox.open( $('.comparison_open'), {
            modal: true,
            'padding'    : 0,
            afterLoad: function() {
                comparison.init();
            }
        });
    });


    $('.reference_toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.reference').find('.hide_part_table').toggleClass('hide');

        if($(this).hasClass('active')) {
            $(this).find('span').text($(this).attr('data-view'));
        }
        else {
            $(this).find('span').text($(this).attr('data-base'));
        }
    });

    $('.person__view').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.person').find('.person__col').toggleClass('view_all');

        if($(this).hasClass('active')) {
            $(this).find('span').text($(this).attr('data-view'));
        }
        else {
            $(this).find('span').text($(this).attr('data-base'));
        }
    });

});



