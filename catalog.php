<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="layout">

            <?php include('inc/header.inc.php') ?>

            <div class="main-breadcrumb">
                <div class="section-wrap">
                    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="#" itemprop="item">Главная</a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="#" itemprop="item">Каталог</a>
                            <meta itemprop="position" content="2">
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <span itemprop="name">Дизельные генераторы 1000 кВт</span>
                            <meta itemprop="position" content="3">
                        </li>
                    </ul>
                </div>
            </div>

            <section class="section section-light-gray">
                <div class="section-wrap">

                    <h1 class="heading">Дизельные электростанции 1000 кВт в Нижнем Новгороде</h1>

                    <div class="filter">
                        <div class="filter__nav">
                            <a href="#" data-target=".filter__tab_01" class="active">Фильтр по цене</a>
                            <a href="#" data-target=".filter__tab_02">Подбор по мощности</a>
                            <a href="#" data-target=".filter__tab_03">Исполнение: <br class="hide-sm"/> кожух, контейнер, шасси</a>
                            <a href="#" data-target=".filter__tab_04">Производитель</a>
                            <a href="#" data-target=".filter__tab_05">Страна сборки модели</a>
                        </div>
                        <div class="filter__main">
                            <div class="filter__content">
                                <div class="filter__tab filter__tab_01 active">
                                    <div class="filter__price">
                                        <ul>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_price" value="1" checked>
                                                    <span>От 8 до 10 млн. рублей</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_price" value="2">
                                                    <span>От 15 до 17 млн. рублей</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_price" value="3">
                                                    <span>От 10 до 15 млн. рублей</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__tab filter__tab_02">
                                    <div class="filter__power">
                                        <ul>
                                            <li><a href="#" class="filter_button">40 кВт</a></li>
                                            <li><a href="#" class="filter_button">150 кВт</a></li>
                                            <li><a type="button" class="filter_button">300 кВт</a></li>
                                            <li><a type="button" class="filter_button">600 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 000 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 300 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 600 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 900 кВт</a></li>
                                            <li><a type="button" class="filter_button">2 200 кВт</a></li>

                                            <li><a type="button" class="filter_button">40 кВт</a></li>
                                            <li><a type="button" class="filter_button">150 кВт</a></li>
                                            <li><a type="button" class="filter_button">300 кВт</a></li>
                                            <li><a type="button" class="filter_button">600 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 000 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 300 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 600 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 900 кВт</a></li>
                                            <li><a type="button" class="filter_button">2 200 кВт</a></li>

                                            <li><a type="button" class="filter_button">40 кВт</a></li>
                                            <li><a type="button" class="filter_button">150 кВт</a></li>
                                            <li><a type="button" class="filter_button">300 кВт</a></li>
                                            <li><a type="button" class="filter_button">600 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 000 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 300 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 600 кВт</a></li>
                                            <li><a type="button" class="filter_button">1 900 кВт</a></li>
                                            <li><a type="button" class="filter_button">2 200 кВт</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__tab filter__tab_03">
                                    <div class="filter__kit">
                                        <div class="filter__kit_wrap">
                                            <div class="filter__title">Для каждой дизельной электростанции доступны опции:</div>
                                            <ul>
                                                <li>
                                                    <a href="#" class="filter_button filter_button_text filter_button_single">
                                                        <strong>по исполнению:</strong> в шумозащитном кожухе, контейнере и на шасси
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="filter_button filter_button_text filter_button_single">
                                                        <strong>по автоматизации:</strong> ручной запуск (1 степень), автоматический запуск (2 степень) и с системами автоматической подкачки топлива и масла (3 степень)
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="filter__kit_text">За точными расчетами цены на опции обращайтесь к специалистам «Техэкспо»</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter__tab filter__tab_04">
                                    <div class="filter__vendor">
                                        <ul>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Наменование компании</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__tab filter__tab_05">
                                    <div class="filter__country">
                                        <ul>
                                            <li>
                                                <label class="filter_item filter_item_flag checked">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Великобритания</span>
                                                    <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item filter_item_flag checked">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Великобритания</span>
                                                    <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item filter_item_flag checked">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Великобритания</span>
                                                    <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item filter_item_flag checked">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Великобритания</span>
                                                    <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="filter_item filter_item_flag checked">
                                                    <input type="checkbox" name="fl_vendor" value="1">
                                                    <span>Великобритания</span>
                                                    <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="filter__reset">
                                <button type="button" class="btn_base btn_base_light">Очистить фильтр</button>
                            </div>
                        </div>
                    </div>

                    <div class="goods-block">
                        <ul class="goods-block__header">
                            <li>Название модели продукции</li>
                            <li><span class="sort">Мощность</span></li>
                            <li>Двигатель</li>
                            <li><span class="sort">Страна сборки</span></li>
                            <li><span class="sort">Цена товара</span></li>
                        </ul>
                        <div class="goods-block__body" itemscope itemtype="http://schema.org/ItemList">

                            <div class="goods" itemscope itemprop="itemListElement" itemtype="http://schema.org/Product">
                                <a itemprop="url" href="" class="goods__image">
                                    <div class="goods__image_inner">
                                        <img itemprop="image src="images/goods__01.jpg" class="img_fluid" alt="">
                                    </div>
                                </a>
                                <div class="goods__info">
                                    <meta itemprop="description" content="описание_товара">
                                    <div class="goods__info_name">
                                        <a href="#" itemprop="name">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2РН Cummins</strong></a>
                                    </div>
                                    <div class="goods__info_availability">Cклад: Нижний Новгород 3 шт.</div>
                                </div>
                                <div class="goods__elem goods__power" data-label="Мощность:">1000 кВт <br/>(1250 кВА)</div>
                                <div class="goods__elem goods__engine" data-label="Двигатель:"><span itemprop="model">Baudouin 6M33G715/5e2</span></div>
                                <div class="goods__elem goods__country" data-label="Страна сборки:">
                                    <div class="country">
                                        <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                        <span>Великобритания</span>
                                    </div>
                                </div>
                                <div class="goods__purchase" itemscope itemprop="offers" itemtype="http://schema.org/Offer">
                                    <div class="goods__price" itemprop="price">14 395 925 руб.</div>
                                    <meta itemprop="priceCurrency" content="RUB">
                                    <meta itemprop="availability" href="http://schema.org/InStock" content="В наличии" />
                                    <a href="#push" class="btn_base btn_base_sm text_strong goods__purchase_checkout btn_modal">Купить</a>
                                    <div class="goods__comparison">
                                        <button class="btn_base btn_base_sm btn_base btn_base_gray goods__comparison_add">Добавить в сравнение</button>
                                        <a class="btn_base btn_base_sm btn_base btn_base_dark goods__comparison_link btn_comparison" href="#">Перейти к сравнению</a>
                                    </div>
                                </div>
                            </div>

                            <div class="goods">
                                <a href="" class="goods__image">
                                    <div class="goods__image_inner">
                                        <img src="images/goods__02.jpg" class="img_fluid" alt="">
                                    </div>
                                </a>
                                <div class="goods__info">
                                    <div class="goods__info_name">
                                        <a href="#">Дизель-генераторная установка <strong>Onis Visa P 650 GX (серия GALAXY, в кожухе)</strong></a>
                                    </div>
                                </div>
                                <div class="goods__elem goods__power" data-label="Мощность:">1000 кВт <br/>(1250 кВА)</div>
                                <div class="goods__elem goods__engine" data-label="Двигатель:"><span>Perkins 2806A-E18TAG2	</span></div>
                                <div class="goods__elem goods__country" data-label="Страна сборки:">
                                    <div class="country">
                                        <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                        <span>Великобритания</span>
                                    </div>
                                </div>
                                <div class="goods__purchase">
                                    <div class="goods__price">14 395 925 руб.</div>
                                    <a href="#push" class="btn_base btn_base_sm text_strong goods__purchase_checkout btn_modal">Заказать</a>
                                    <div class="goods__comparison">
                                        <button class="btn_base btn_base_sm btn_base btn_base_gray goods__comparison_add">Добавить в сравнение</button>
                                        <a class="btn_base btn_base_sm btn_base btn_base_dark goods__comparison_link btn_comparison" href="#">Перейти к сравнению</a>
                                    </div>
                                </div>
                            </div>

                            <div class="goods">
                                <a href="" class="goods__image">
                                    <div class="goods__image_inner">
                                        <img src="images/goods__03.jpg" class="img_fluid" alt="">
                                    </div>
                                </a>
                                <div class="goods__info">
                                    <div class="goods__info_name">
                                        <a href="#">Дизельная электростанция <strong>Green Power GP1380A/M</strong></a>
                                    </div>
                                    <div class="goods__info_availability">Cклад: Нижний Новгород 3 шт.		</div>
                                </div>
                                <div class="goods__elem goods__power" data-label="Мощность:">1000 кВт <br/>(1250 кВА)</div>
                                <div class="goods__elem goods__engine" data-label="Двигатель:"><span>Mitsubishi S12R-PTA</span></div>
                                <div class="goods__elem goods__country" data-label="Страна сборки:">
                                    <div class="country">
                                        <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                        <span>Великобритания</span>
                                    </div>
                                </div>
                                <div class="goods__purchase">
                                    <div class="goods__price">14 395 925 руб.</div>
                                    <a href="#push" class="btn_base btn_base_sm text_strong goods__purchase_checkout btn_modal">Купить</a>
                                    <div class="goods__comparison">
                                        <button class="btn_base btn_base_sm btn_base btn_base_gray goods__comparison_add">Добавить в сравнение</button>
                                        <a class="btn_base btn_base_sm btn_base btn_base_dark goods__comparison_link btn_comparison" href="#">Перейти к сравнению</a>
                                    </div>
                                </div>
                            </div>

                            <div class="goods">
                                <a href="" class="goods__image">
                                    <div class="goods__image_inner">
                                        <img src="images/goods__04.jpg" class="img_fluid" alt="">
                                    </div>
                                </a>
                                <div class="goods__info">
                                    <div class="goods__info_name">
                                        <a href="#">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2РН Cummins</strong></a>
                                    </div>
                                    <div class="goods__info_availability">Cклад: Нижний Новгород 3 шт.</div>
                                </div>
                                <div class="goods__elem goods__power" data-label="Мощность:">1000 кВт <br/>(1250 кВА)</div>
                                <div class="goods__elem goods__engine" data-label="Двигатель:"><span>Baudouin 6M33G715/5e2</span></div>
                                <div class="goods__elem goods__country" data-label="Страна сборки:">
                                    <div class="country">
                                        <i><img src="img/flag/flag__uk.svg" class="img_fluid" alt=""></i>
                                        <span>Великобритания</span>
                                    </div>
                                </div>
                                <div class="goods__purchase">
                                    <div class="goods__price">Цена по запросу</div>
                                    <a href="#push" class="btn_base btn_base_sm text_strong goods__purchase_checkout btn_modal">Запросить</a>
                                    <div class="goods__comparison">
                                        <button class="btn_base btn_base_sm btn_base btn_base_gray goods__comparison_add">Добавить в сравнение</button>
                                        <a class="btn_base btn_base_sm btn_base btn_base_dark goods__comparison_link btn_comparison" href="#">Перейти к сравнению</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/completed.inc.php') ?>

            <?php include('inc/office.inc.php') ?>

            <?php include('inc/selection.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/modal.inc.php') ?>
        
        <!-- Comparison -->
        <div class="hide">
            <a href="#comparison" class="comparison_open"></a>
            <div class="comparison" id="comparison">
                <div class="btn_hide_modal" data-fancybox-close></div>
                <div class="comparison__wrap">
                    <ul class="comparison__legend">
                        <li><span>Сравниваемые модели</span></li>
                        <li><span>Страна сборки</span></li>
                        <li><span>Производитель:</span></li>
                        <li><span>Номинальная мощность:</span></li>
                        <li class="double_height"><span>Модель двигателя:</span></li>
                        <li><span>Страна производства двигателя:</span></li>
                        <li><span>Объём двигателя:</span></li>
                        <li><span>Расход топлива при 75% нагрузке:</span></li>
                        <li><span>Расположение цилиндров:</span></li>
                        <li><span>Габариты ДЭС, мм:</span></li>
                        <li><span>Масса:</span></li>
                    </ul>
                    <div class="comparison__main">
                        <div class="comparison__content">
                            <div class="comparison__slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="comparison-goods">
                                            <div class="comparison-goods__image">
                                                <a href="#" class="comparison-goods__image_item">
                                                    <img src="images/comparison__image__01.jpg" class="img_fluid" alt="">
                                                    <button class="comparison-goods__remove"></button>
                                                </a>
                                            </div>
                                            <div class="comparison-goods__title">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2 Н Cummins</strong></div>
                                            <div class="comparison-goods__price">14 395 925 руб.</div>
                                            <ul class="comparison-goods__info">
                                                <li><span>Техэкспо</span></li>
                                                <li><span>Россия</span></li>
                                                <li class="double_height"><span>1000 кВт</span></li>
                                                <li class="double_height"><span>Perkins 4012-46TWG2A</li>
                                                <li class="double_height"><span>Великобритания</span></li>
                                                <li><span>49 л</span></li>
                                                <li class="double_height"><span>200 л/час</span></li>
                                                <li class="double_height"><span>12, V-образное</span></li>
                                                <li><span>4500x2050x2350</span></li>
                                                <li><span>10 000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="comparison-goods">
                                            <div class="comparison-goods__image">
                                                <a href="#" class="comparison-goods__image_item">
                                                    <img src="images/comparison__image__01.jpg" class="img_fluid" alt="">
                                                    <button class="comparison-goods__remove"></button>
                                                </a>
                                            </div>
                                            <div class="comparison-goods__title">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2 Н Cummins</strong></div>
                                            <div class="comparison-goods__price">14 395 925 руб.</div>
                                            <ul class="comparison-goods__info">
                                                <li><span>Техэкспо</span></li>
                                                <li><span>Россия</span></li>
                                                <li class="double_height"><span>1000 кВт</span></li>
                                                <li class="double_height"><span>Perkins 4012-46TWG2A</li>
                                                <li class="double_height"><span>Великобритания</span></li>
                                                <li><span>49 л</span></li>
                                                <li class="double_height"><span>200 л/час</span></li>
                                                <li class="double_height"><span>12, V-образное</span></li>
                                                <li><span>4500x2050x2350</span></li>
                                                <li><span>10 000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="comparison-goods">
                                            <div class="comparison-goods__image">
                                                <a href="#" class="comparison-goods__image_item">
                                                    <img src="images/comparison__image__01.jpg" class="img_fluid" alt="">
                                                    <button class="comparison-goods__remove"></button>
                                                </a>
                                            </div>
                                            <div class="comparison-goods__title">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2 Н Cummins</strong></div>
                                            <div class="comparison-goods__price">14 395 925 руб.</div>
                                            <ul class="comparison-goods__info">
                                                <li><span>Техэкспо</span></li>
                                                <li><span>Россия</span></li>
                                                <li class="double_height"><span>1000 кВт</span></li>
                                                <li class="double_height"><span>Perkins 4012-46TWG2A</li>
                                                <li class="double_height"><span>Великобритания</span></li>
                                                <li><span>49 л</span></li>
                                                <li class="double_height"><span>200 л/час</span></li>
                                                <li class="double_height"><span>12, V-образное</span></li>
                                                <li><span>4500x2050x2350</span></li>
                                                <li><span>10 000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="comparison-goods">
                                            <div class="comparison-goods__image">
                                                <a href="#" class="comparison-goods__image_item">
                                                    <img src="images/comparison__image__01.jpg" class="img_fluid" alt="">
                                                    <button class="comparison-goods__remove"></button>
                                                </a>
                                            </div>
                                            <div class="comparison-goods__title">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2 Н Cummins</strong></div>
                                            <div class="comparison-goods__price">14 395 925 руб.</div>
                                            <ul class="comparison-goods__info">
                                                <li><span>Техэкспо</span></li>
                                                <li><span>Россия</span></li>
                                                <li class="double_height"><span>1000 кВт</span></li>
                                                <li class="double_height"><span>Perkins 4012-46TWG2A</li>
                                                <li class="double_height"><span>Великобритания</span></li>
                                                <li><span>49 л</span></li>
                                                <li class="double_height"><span>200 л/час</span></li>
                                                <li class="double_height"><span>12, V-образное</span></li>
                                                <li><span>4500x2050x2350</span></li>
                                                <li><span>10 000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="comparison-goods">
                                            <div class="comparison-goods__image">
                                                <a href="#" class="comparison-goods__image_item">
                                                    <img src="images/comparison__image__01.jpg" class="img_fluid" alt="">
                                                    <button class="comparison-goods__remove"></button>
                                                </a>
                                            </div>
                                            <div class="comparison-goods__title">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2 Н Cummins</strong></div>
                                            <div class="comparison-goods__price">14 395 925 руб.</div>
                                            <ul class="comparison-goods__info">
                                                <li><span>Техэкспо</span></li>
                                                <li><span>Россия</span></li>
                                                <li class="double_height"><span>1000 кВт</span></li>
                                                <li class="double_height"><span>Perkins 4012-46TWG2A</li>
                                                <li class="double_height"><span>Великобритания</span></li>
                                                <li><span>49 л</span></li>
                                                <li class="double_height"><span>200 л/час</span></li>
                                                <li class="double_height"><span>12, V-образное</span></li>
                                                <li><span>4500x2050x2350</span></li>
                                                <li><span>10 000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="comparison-goods">
                                            <div class="comparison-goods__image">
                                                <a href="#" class="comparison-goods__image_item">
                                                    <img src="images/comparison__image__01.jpg" class="img_fluid" alt="">
                                                    <button class="comparison-goods__remove"></button>
                                                </a>
                                            </div>
                                            <div class="comparison-goods__title">Дизельная электростанция <strong>Техэкспо ТЭ.504С-Т400-2 Н Cummins</strong></div>
                                            <div class="comparison-goods__price">14 395 925 руб.</div>
                                            <ul class="comparison-goods__info">
                                                <li><span>Техэкспо</span></li>
                                                <li><span>Россия</span></li>
                                                <li class="double_height"><span>1000 кВт</span></li>
                                                <li class="double_height"><span>Perkins 4012-46TWG2A</li>
                                                <li class="double_height"><span>Великобритания</span></li>
                                                <li><span>49 л</span></li>
                                                <li class="double_height"><span>200 л/час</span></li>
                                                <li class="double_height"><span>12, V-образное</span></li>
                                                <li><span>4500x2050x2350</span></li>
                                                <li><span>10 000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
