<footer class="footer">
    <div class="section-wrap">
        <div class="footer__row">

            <div class="footer__info">
                <a class="footer__logo" href="/">
                    <img src="img/footer__logo.svg" class="img_fluid" alt="">
                </a>
                <div class="footer__text">
                    <p>Производство ДГУ и проектирование систем энергоснабжения в Нижнем Новгороде.</p>
                    <p>Информация, изложенная на сайте, не является публичной офертой.</p>
                    <p>Заполняя любую форму на сайте tech-expo.ru, вы даете согласие на обработку персональных данных.</p>
                </div>
                <div class="footer__copy">© 2013-2020 ООО «Техэкспо»</div>
            </div>

            <div class="footer__center">
                <div class="footer__center_column">
                    <div class="footer__block">
                        <div class="footer__title">Навигация:</div>
                        <ul class="footer__nav">
                            <li><a href="#">О компании</a></li>
                            <li><a href="#">Закупки ДГУ по 44-ФЗ и 223-ФЗ</a></li>
                            <li><a href="#">Склад</a></li>
                            <li><a href="#">Доставка</a></li>
                            <li><a href="#">Законодательство</a></li>
                        </ul>
                    </div>
                    <div class="footer__block hide-xs-only hide-sm-only">
                        <div class="footer__title">Сертификаты:</div>
                        <ul class="footer__nav">
                            <li><a href="#">ISO 9001:2015</a></li>
                            <li><a href="#">СРО-П-161-09092010, СРО-С-258-11012013</a></li>
                            <li><a href="#">Электролаборатория №14-53/ЭЛ-19</a></li>
                            <li><a href="#">OHSAS 18000:2007</a></li>
                        </ul>
                    </div>
                </div>
                <div class="footer__center_column">
                    <div class="footer__block text_nowrap">
                        <div class="footer__title">Наши реквизиты:</div>
                        <ul class="footer__prop">
                            <li>ООО «Техэкспо»</li>
                            <li>ИНН 7840490040</li>
                            <li>КПП 780501001</li>
                            <li>ОГРН 1137847211886</li>
                        </ul>
                        <a class="footer__prop_link" href="#">Скачать реквизиты »</a>
                    </div>
                    <div class="footer__block hide-xs-only hide-sm-only">
                        <div class="footer__title">Мы в социальных сетях:</div>
                        <div class="footer__social">
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 46 46"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 46 46"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__instagram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer__contact">
                <div class="footer__block">
                    <div class="footer__title">Наш адрес:</div>
                    <div class="footer__address">
                        <div class="footer__address_city">
                            <i>
                                <svg class="ico_svg" viewBox="0 0 14 18"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__place" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <span>Нижний новгород</span>
                        </div>
                        <div class="footer__address_street">ул. Максима Горького, д. 260</div>
                    </div>
                </div>
                <div class="footer__block">
                    <div class="footer__title">Электронная почта:</div>
                    <a class="footer__email">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 19 20"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__email" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>order+07381@tech-expo.ru</span>
                    </a>
                </div>
                <div class="footer__block">
                    <div class="footer__title">Телефон и время работы:</div>
                    <ul class="footer__touch">
                        <li>
                            <a href="tel:88312885450" class="footer__touch_item">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__phone" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>8  831  288 - 54 - 50</span>
                            </a>
                        </li>
                        <li>
                            <div class="footer__touch_item">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__clock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Пн-Вс  8:00 - 18:00</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="footer__block hide-xs-only hide-md">
                    <div class="footer__title">Мы в социальных сетях:</div>
                    <div class="footer__social">
                        <a href="#">
                            <svg class="ico_svg" viewBox="0 0 46 46"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </a>
                        <a href="#">
                            <svg class="ico_svg" viewBox="0 0 46 46"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__instagram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="section-wrap">© 2013-2020 ООО «Техэкспо»</div>
    </div>
</footer>