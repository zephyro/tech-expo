<header class="main-header">

    <div class="topnav">
        <div class="section-wrap">
            <div class="topnav__row">
                <ul class="topnav__menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
                    <li><a href="#" itemprop="url">О компании</a></li>
                    <li><a href="#" itemprop="url">Выполненные проекты</a></li>
                    <li><a href="#" itemprop="url">Производство</a></li>
                    <li><a href="#" itemprop="url">В наличии</a></li>
                    <li><a href="#" itemprop="url">Доставка</a></li>
                    <li><a href="#" itemprop="url">У Вас тендер?</a></li>
                    <li><a href="#" itemprop="url">Контакты</a></li>
                </ul>
                <div class="topnav__search">
                    <div class="topnav__search_icon">
                        <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="topnav__search_form">
                        <form class="form">
                            <input type="text" name="search" class="topnav__search_input" placeholder="Введите фразу для поиска">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="head" itemscope itemtype="http://schema.org/Organization">
        <div class="section-wrap">
            <div class="hide" itemprop="name">ООО «Техэкспо»</div>
            <link itemprop="url" href="https://tech-expo.ru/">
            <div class="head__row">
                <button class="head__toggle nav_toggle" type="button">
                    <span></span>
                </button>

                <a href="/" class="head__logo">
                    <div class="head__logo_image" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                        <img itemprop="contentUrl" src="img/logo.svg" class="img_fluid" alt="">
                        <meta itemprop="width" content="278">
                        <meta itemprop="height" content="40">
                    </div>
                    <div class="head__logo_text">Производство дизельных электростанций <br/>и энергокомплексов до 30 МВт</div>
                </a>

                <div class="head__box head__box_address" itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                    <div class="head__title">Выбранный город:</div>
                    <a href="#" class="head__city city_toggle">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 14 18"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__place" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span class="head__city_active" itemprop="addressLocality">Нижний Новгород</span>
                        <b>
                            <svg class="ico_svg" viewBox="0 0 8 6"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                    </a>
                    <div class="head__address" itemprop="streetAddress">ул. Максима Горького, д. 260</div>
                </div>

                <div class="head__box head__box_info">
                    <div class="head__title">Тел. и время работы:</div>
                    <a class="head__tel" href="tel:+88312885450">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__phone" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>8 831 288 - 54 - 50</span>
                    </a>
                    <a href="#" class="head__worktime">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__clock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Пн-Вс  8:00 - 18:00</span>
                    </a>
                </div>

                <div class="head__box head__box_email">
                    <div class="head__title">Электронная почта:</div>
                    <a class="head__email" href="mailto:order07381@tech-expo.ru">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 19 20"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__email" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span itemprop="email">order07381@tech-expo.ru</span>
                    </a>
                    <a href="#order" class="head__order btn_modal">Оставить свою заявку</a>
                </div>

                <div class="head__box head__box_contact">
                    <div class="head__title">Звонок по России бесплатно</div>
                    <a class="head__phone" href="tel:+78312885450" itemprop="telephone">+7 (831) 288-54-50</a>
                    <a href="#callback" class="head__order btn_modal">Заказать обратный звонок</a>
                </div>

                <div class="head__search">
                    <button class="head__search_button search_toggle" type="button">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <div class="head__search_content">
                        <div class="head__search_wrap">
                            <button class="head__search_close search_toggle"></button>
                            <form class="form">
                                <input type="text" name="search" class="head__search_input" placeholder="">
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="city-switch">
        <div class="section-wrap">
            <div class="cities-wrapper">
                <a class="city" href="//tech-expo.ru/" id="bx_3218110189_39215">
                    <ul>
                        <li>Санкт-Петербург</li>
                        <li>Промышленная ул., д. 19Р, офис 103-С (БЦ «Редуктор»)</li>
                        <li>+7 812 602-52-94</li>
                    </ul>
                </a>
                <a class="city" href="//moscow.tech-expo.ru/" id="bx_3218110189_36952">
                    <ul>
                        <li>Москва</li>
                        <li>Новгородская ул., д. 1</li>
                        <li>+7 499 647-54-32</li>
                    </ul>
                </a>
                <a class="city" href="//volgograd.tech-expo.ru/" id="bx_3218110189_36953">
                    <ul>
                        <li>Волгоград</li>
                        <li>Мира ул., д. 19</li>
                        <li>+7 844 268-48-25</li>
                    </ul>
                </a>
                <a class="city" href="//voronezh.tech-expo.ru/" id="bx_3218110189_36954">
                    <ul>
                        <li>Воронеж</li>
                        <li>Московский пр., д. 4</li>
                        <li>+7 473 201-60-99</li>
                    </ul>
                </a>
                <a class="city" href="//ekaterinburg.tech-expo.ru/" id="bx_3218110189_36955">
                    <ul>
                        <li>Екатеринбург</li>
                        <li>Антона Валека ул., д. 13</li>
                        <li>+7 343 302-00-42</li>
                    </ul>
                </a>
                <a class="city" href="//kazan.tech-expo.ru/" id="bx_3218110189_36956">
                    <ul>
                        <li>Казань</li>
                        <li>Проточная ул., д. 8</li>
                        <li>+7 843 207-28-35</li>
                    </ul>
                </a>
                <a class="city" href="//krasnodar.tech-expo.ru/" id="bx_3218110189_36957">
                    <ul>
                        <li>Краснодар</li>
                        <li>Карасунская ул., д. 60</li>
                        <li>+7 861 211-72-34</li>
                    </ul>
                </a>
                <a class="city" href="//krasnoyarsk.tech-expo.ru/" id="bx_3218110189_36958">
                    <ul>
                        <li>Красноярск</li>
                        <li>Взлётная ул., д. 57</li>
                        <li>+7 391 229-59-39</li>
                    </ul>
                </a>
                <a class="city" href="//nizhny-novgorod.tech-expo.ru/" id="bx_3218110189_36959">
                    <ul>
                        <li>Нижний Новгород</li>
                        <li>Максима Горького, д. 260</li>
                        <li>+7 831 288-54-50</li>
                    </ul>
                </a>
                <a class="city" href="//novosibirsk.tech-expo.ru/" id="bx_3218110189_36960">
                    <ul>
                        <li>Новосибирск</li>
                        <li>Гаранина ул., д. 15</li>
                        <li>+7 383 312-14-04</li>
                    </ul>
                </a>
                <a class="city" href="//orenburg.tech-expo.ru/" id="bx_3218110189_36961">
                    <ul>
                        <li>Оренбург</li>
                        <li>Шоссейная ул., 24А</li>
                        <li>+7 353 248-64-94</li>
                    </ul>
                </a>
                <a class="city" href="//perm.tech-expo.ru/" id="bx_3218110189_36962">
                    <ul>
                        <li>Пермь</li>
                        <li>Аркадия Гайдара ул., д. 8Б</li>
                        <li>+7 342 233-83-04</li>
                    </ul>
                </a>
                <a class="city" href="//rostov-na-donu.tech-expo.ru/" id="bx_3218110189_36963">
                    <ul>
                        <li>Ростов-на-Дону</li>
                        <li>Максима Горького ул., д. 295</li>
                        <li>+7 863 309-21-51</li>
                    </ul>
                </a>
                <a class="city" href="//samara.tech-expo.ru/" id="bx_3218110189_36964">
                    <ul>
                        <li>Самара</li>
                        <li>Скляренко ул., д. 26</li>
                        <li>+7 846 215-16-17</li>
                    </ul>
                </a>
                <a class="city" href="//surgut.tech-expo.ru/" id="bx_3218110189_36965">
                    <ul>
                        <li>Сургут</li>
                        <li>30 лет Победы ул., 44Б</li>
                        <li>+7 346 276-92-88</li>
                    </ul>
                </a>
                <a class="city" href="//tyumen.tech-expo.ru/" id="bx_3218110189_36966">
                    <ul>
                        <li>Тюмень</li>
                        <li>Пермякова ул., д. 1</li>
                        <li>+7 345 256-43-32</li>
                    </ul>
                </a>
                <a class="city" href="//ufa.tech-expo.ru/" id="bx_3218110189_36967">
                    <ul>
                        <li>Уфа</li>
                        <li>Кирова ул, д. 107</li>
                        <li>+7 347 225-34-97</li>
                    </ul>
                </a>
                <a class="city" href="//chelyabinsk.tech-expo.ru/" id="bx_3218110189_36968">
                    <ul>
                        <li>Челябинск</li>
                        <li>Победы пр., д. 160</li>
                        <li>+7 351 225-72-62</li>
                    </ul>
                </a>
                <a class="city" href="//yakutsk.tech-expo.ru/" id="bx_3218110189_36969">
                    <ul>
                        <li>Якутск</li>
                        <li>Короленко ул., 25</li>
                        <li>+7 411 250-55-80</li>
                    </ul>
                </a>
                <a class="city" href="//yaroslavl.tech-expo.ru/" id="bx_3218110189_36970">
                    <ul>
                        <li>Ярославль</li>
                        <li>Некрасова ул., д. 41А</li>
                        <li>+7 4852 27-52-34</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>

    <div class="main-nav">
        <div class="section-wrap">
            <ul itemscope itemtype="http://schema.org/SiteNavigationElement">
                <li><a href="#" itemprop="url">Дизельные электростанции</a></li>
                <li><a href="#" itemprop="url">Энергокомплексы 3-50 МВт</a></li>
                <li><a href="#" itemprop="url">Контейнеры для ДГУ</a></li>
                <li><a href="#" itemprop="url">ИБП</a></li>
                <li><a href="#" itemprop="url">Нагрузочные модули</a></li>
                <li><a href="#" itemprop="url">Услуги</a></li>
            </ul>
        </div>
    </div>

    <div class="head-mobile">
        <div class="head__wrap">
            <span>Звонок по всей России беплатный:</span>
            <a href="tel:88312885450">8 (831) 288 54 50</a>
        </div>
    </div>
</header>

<div class="nav-mobile">

    <div class="nav-mobile__contact">
        <div class="nav-mobile__address">
            <div class="head__title hide-xs-only">Выбранный город:</div>
            <a href="#" class="head__city">
                <i>
                    <svg class="ico_svg" viewBox="0 0 14 18"  xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__place" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
                <span class="head__city_active city_toggle">Нижний новгород</span>
                <b>
                    <svg class="ico_svg" viewBox="0 0 8 6"  xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </b>
            </a>
            <div class="head__address">ул. Максима Горького, д. 260</div>
        </div>
        <div class="nav-mobile__email">
            <div class="head__title">Электронная почта:</div>
            <a class="head__email" href="mailto:order07381@tech-expo.ru">
                <i>
                    <svg class="ico_svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__email" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
                <span>order07381@tech-expo.ru</span>
            </a>
            <a href="#order" class="head__order btn_modal">Оставить свою заявку</a>
        </div>
    </div>
    <ul class="nav-mobile__menu">
        <li>
            <a href="#">
                <span>Дизельные электростанции</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Энергокомплексы 3-50 МВт</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Контейнеры для ДГУ</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>ИБП</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Нагрузочные модули</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Услуги</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>

        <li>
            <a href="#">
                <span>О компании</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Выполненные проекты</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Производство</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>В наличии</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Доставка</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>У Вас тендер?</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Контакты</span>
                <i>
                    <svg class="ico_svg" viewBox="0 0 8 13" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
            </a>
        </li>
    </ul>
</div>
<div class="nav-mobile__layout nav_toggle"></div>
