<div class="selection">
    <div class="section-wrap">
        <div class="selection__header">Дизельные электростанции:</div>
        <div class="selection__nav">
            <a href="#" data-target=".selection__tab_01" class="active">По мощности</a>
            <a href="#" data-target=".selection__tab_02">По производителю</a>
            <a href="#" data-target=".selection__tab_03">По двигателю	</a>
            <a href="#" data-target=".selection__tab_04">По цене	</a>
        </div>
        <div class="selection__content">
            <div class="selection__scroll">
                <div class="selection__tab selection__tab_01 active">
                    <ul class="selection__power">
                        <li>
                            <div class="selection__power_title">
                                Дизельные генераторы<br/>
                                40-100 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">40 кВт</a>
                                <a href="#">80 кВт</a>
                                <a href="#">100 кВт</a>
                            </div>
                        </li>
                        <li>
                            <div class="selection__power_title">
                                Дизельные генераторы<br/>
                                300-500 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">300 кВт</a>
                                <a href="#">400 кВт</a>
                                <a href="#">500 кВт</a>
                            </div>
                        </li>
                        <li>
                            <div class="selection__power_title">
                                Дизельные электростанции<br/>
                                1 000-1 200к кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">1 000 кВт</a>
                                <a href="#">1 100 кВт</a>
                                <a href="#">1 200 кВт</a>
                            </div>
                        </li>
                        <li>
                            <div class="selection__power_title">
                                Дизельные электростанции<br/>
                                1 600-1 800 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">1 600 кВт</a>
                                <a href="#">1 700 кВт </a>
                                <a href="#">1 800 кВт</a>
                            </div>
                        </li>

                        <li>
                            <div class="selection__power_title">
                                Дизельные генераторы<br/>
                                150-250 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">150 кВт</a>
                                <a href="#">200 кВт</a>
                                <a href="#">250 кВт</a>
                            </div>
                        </li>
                        <li>
                            <div class="selection__power_title">
                                Дизельные генераторы<br/>
                                600-800 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">600 кВт</a>
                                <a href="#">700 кВт</a>
                                <a href="#">800 кВт</a>
                            </div>
                        </li>
                        <li>
                            <div class="selection__power_title">
                                Дизельные генераторы<br/>
                                1 300-1 500 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">1300 кВт</a>
                                <a href="#">1400 кВт</a>
                                <a href="#">1500 кВт</a>
                            </div>
                        </li>
                        <li>
                            <div class="selection__power_title">
                                Дизельные генераторы<br/>
                                2 000-2 500 кВт
                            </div>
                            <div class="selection__power_links">
                                <a href="#">2000 кВт</a>
                                <a href="#">2200 кВт</a>
                                <a href="#">25000 кВт</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="selection__tab selection__tab_02">
                    <ul class="selection__brand">
                        <li><a href="#" class="selection__button">Техэкспо</a></li>
                        <li><a href="#" class="selection__button">EMSA</a></li>
                        <li><a href="#" class="selection__button">Green Power</a></li>
                        <li><a href="#" class="selection__button">Mitsubishi</a></li>
                        <li><a href="#" class="selection__button">SDMO</a></li>
                        <li><a href="#" class="selection__button">AKSA</a></li>
                        <li><a href="#" class="selection__button">Energo</a></li>
                        <li><a href="#" class="selection__button">HERTZ</a></li>
                        <li><a href="#" class="selection__button">MOTOR</a></li>
                        <li><a href="#" class="selection__button">Teksan</a></li>
                        <li><a href="#" class="selection__button">CTM</a></li>
                        <li><a href="#" class="selection__button">Fogo</a></li>
                        <li><a href="#" class="selection__button">Himoinsa</a></li>
                        <li><a href="#" class="selection__button">Onis Visa</a></li>
                        <li><a href="#" class="selection__button">Tide Power</a></li>
                        <li><a href="#" class="selection__button">Cummins</a></li>
                        <li><a href="#" class="selection__button">FPT (Iveco)</a></li>
                        <li><a href="#" class="selection__button">Inmesol</a></li>
                        <li><a href="#" class="selection__button">PowerLink</a></li>
                        <li><a href="#" class="selection__button">Wärtsilä</a></li>
                        <li><a href="#" class="selection__button">Denyo</a></li>
                        <li><a href="#" class="selection__button">Geko</a></li>
                        <li><a href="#" class="selection__button">JCB</a></li>
                        <li><a href="#" class="selection__button">Pramac</a></li>
                        <li><a href="#" class="selection__button">Weichai</a></li>
                        <li><a href="#" class="selection__button">Elcos</a></li>
                        <li><a href="#" class="selection__button">Gesan</a></li>
                        <li><a href="#" class="selection__button">MAN</a></li>
                        <li><a href="#" class="selection__button">RID</a></li>
                        <li><a href="#" class="selection__button">Wilson</a></li>
                    </ul>
                </div>
                <div class="selection__tab selection__tab_03">
                    <div class="selection__engine">
                        <div class="selection__engine_group">
                            <div class="selection__engine_title">Бюджетные:</div>
                            <ul class="selection__engine_list">
                                <li><a href="#" class="selection__button">Baudouin</a></li>
                                <li><a href="#" class="selection__button">Weichai</a></li>
                                <li><a href="#" class="selection__button">Ricardo</a></li>
                                <li><a href="#" class="selection__button">EMSA</a></li>
                                <li><a href="#" class="selection__button">TSS Diesel</a></li>
                                <li><a href="#" class="selection__button">Jichai</a></li>
                                <li><a href="#" class="selection__button">ЯМЗ</a></li>
                            </ul>
                        </div>
                        <div class="selection__engine_group">
                            <div class="selection__engine_title">Оптимальные по качеству и цене:</div>
                            <ul class="selection__engine_list">
                                <li><a href="#" class="selection__button">FPT (Iveco)</a></li>
                                <li><a href="#" class="selection__button">Perkins</a></li>
                                <li><a href="#" class="selection__button">Doosan</a></li>
                                <li><a href="#" class="selection__button">Scania</a></li>
                                <li><a href="#" class="selection__button">Deutz</a></li>
                                <li><a href="#" class="selection__button">Volvo</a></li>
                                <li><a href="#" class="selection__button">Mitsubishi</a></li>
                            </ul>
                        </div>
                        <div class="selection__engine_group">
                            <div class="selection__engine_title">Премиальные:</div>
                            <ul class="selection__engine_list">
                                <li><a href="#" class="selection__button">Cummins</a></li>
                                <li><a href="#" class="selection__button">John Deere</a></li>
                                <li><a href="#" class="selection__button">MTU</a></li>
                                <li><a href="#" class="selection__button">Caterpillar</a></li>
                                <li><a href="#" class="selection__button">MAN</a></li>
                                <li><a href="#" class="selection__button">ABC</a></li>
                                <li><a href="#" class="selection__button">Wärtsilä</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="selection__tab selection__tab_04">
                    <div class="selection__price">
                        <ul class="selection__price_group">
                            <li><a class="selection__button" href="#">Дизель-генераторы до 1 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизель-генераторы до 1.5 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизель-генераторы до 2 млн. руб.</a></li>
                        </ul>
                        <ul class="selection__price_group">
                            <li><a class="selection__button" href="#">Дизельные электростанции 2-3 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 3-4 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 4-5 млн. руб.</a></li>
                        </ul>
                        <ul class="selection__price_group">
                            <li><a class="selection__button" href="#">Дизельные электростанции 5-7 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 8-10 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 10-12 млн. руб.</a></li>
                        </ul>
                        <ul class="selection__price_group">
                            <li><a class="selection__button" href="#">Дизельные электростанции 13-15 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 15-18 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 18-20 млн. руб.</a></li>
                        </ul>
                        <ul class="selection__price_group">
                            <li><a class="selection__button" href="#">Дизельные электростанции 20-25 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 25-30 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 30-40 млн. руб.</a></li>
                        </ul>
                        <ul class="selection__price_group">
                            <li><a class="selection__button" href="#">Дизельные электростанции 40-50 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции 50-70 млн. руб.</a></li>
                            <li><a class="selection__button" href="#">Дизельные электростанции от 70 млн. руб.</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
