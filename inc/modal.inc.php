<div class="hide">

    <div class="modal-content" id="callback">
        <div class="modal-header">
            <h5 class="modal-title">Заказать звонок</h5>
            <span class="close" data-fancybox-close>
                    <span aria-hidden="true">×</span>
                </span>
        </div>
        <div class="modal-body">
            <div id="modal-callback-result"></div>
            <form class="callback-form">
                <div>
                    <div class="form-row name-row">
                        <input type="text" class="form-control client-name" placeholder="Имя" name="form_text_1" required="" minlength="3">
                    </div>

                    <div class="form-row phone-row">
                        <input type="tel" class="form-control client-phone" placeholder="Телефон" name="form_text_2" required="">
                    </div>
                </div>
                <div class="form-row">
                    <button type="submit">Отправить запрос</button>
                </div>
            </form>
        </div>
    </div>


    <div class="modal-content" id="order">
        <div class="modal-header">
            <h5 class="modal-title">Оставить свою заявку</h5>
            <span class="close" data-fancybox-close>
                    <span aria-hidden="true">×</span>
                </span>
        </div>
        <div class="modal-body">
            <div class="modal-callback-result"></div>
            <form class="callback-form">
                <div>
                    <div class="form-row name-row">
                        <input type="text" class="form-control client-name" placeholder="Имя" name="form_text_1" required="" minlength="3">
                    </div>

                    <div class="form-row phone-row">
                        <input type="tel" class="form-control client-phone" placeholder="Телефон" name="form_text_2" required="">
                    </div>
                </div>
                <div class="form-row">
                    <button type="submit">Оставить заявку</button>
                </div>
            </form>
        </div>
    </div>


    <div class="modal-content" id="push">
        <div class="modal-header">
            <h5 class="modal-title">Заявка</h5>
            <span class="close" data-fancybox-close>
                    <span aria-hidden="true">×</span>
                </span>
        </div>
        <div class="modal-body" id="modal-commercial-body">
            <div id="modal-commercial-result"></div>
            <form class="commercial-form" id="modal-commercial-validate" novalidate="novalidate">
                <div id="modal-commercial-fields">
                    <div class="form-row name-row">
                        <input type="text" class="form-control client-name" placeholder="Имя" name="form_text_10" required="" minlength="3">
                    </div>

                    <div class="form-row email-row">
                        <input type="email" class="form-control" placeholder="E-mail" name="form_email_12" required="">
                    </div>

                    <div class="form-row phone-row">
                        <input type="tel" class="form-control client-phone" placeholder="Телефон" name="form_text_11" required="">
                    </div>

                    <div class="form-row textarea-row">
                        <textarea id="app-message" class="form-control" placeholder="Сообщение" name="form_textarea_13" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <button type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>

</div>