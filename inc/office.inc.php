<section class="office">
    <div class="section-wrap">
        <div class="office__header">Офис продаж дизельных электростанций в Нижнем Новгороде</div>
        <div class="office__box">
            <div class="office__content">
                <div class="office__address">
                    <div class="office__title">Наш адрес:</div>
                    <div class="office__address_city">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 14 18"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__place" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Нижний Новгород</span>
                    </div>
                    <div class="office__address_street">ул. Максима Горького, д. 260</div>
                </div>

                <div class="office__contact">
                    <div class="office__title">Телефон и время работы:</div>
                    <a class="office__item"  href="tel:88312885450" >
                        <i>
                            <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__phone" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>8  831  288 - 54 - 50</span>
                    </a>
                    <div class="office__item">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__clock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Пн-Вс  8:00 - 18:00</span>
                    </div>

                </div>
            </div>
            <div class="office__map">
                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A1467c4ada752f25de84da9b9b283a0e9d6384e4e1a4fff91bcd8c3b1830089d4&amp;source=constructor" width="100%" height="264" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</section>