<section class="completed">
    <div class="section-wrap">
        <div class="completed__header">
            <div class="hide-md">Выполненные проекты</div>
            <div class="hide-xs-only hide-sm-only">Выполненные поставки дизель-генераторов мощностью 1000 кВт</div>
        </div>

        <div class="completed__mobile">
            <div class="swiper-container completed__slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <a href="#" class="work">
                            <div class="work__header">
                                <div class="work__image" style="background-image: url('images/work__01.jpg');"></div>
                                <div class="work__info">
                                    <div class="work__date">Февраль 2020 года</div>
                                    <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                                </div>
                            </div>
                            <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#" class="work">
                            <div class="work__header">
                                <div class="work__image" style="background-image: url('images/work__02.jpg');"></div>
                                <div class="work__info">
                                    <div class="work__date">Февраль 2020 года</div>
                                    <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                                </div>
                            </div>
                            <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#" class="work">
                            <div class="work__header">
                                <div class="work__image" style="background-image: url('images/work__03.jpg');"></div>
                                <div class="work__info">
                                    <div class="work__date">Февраль 2020 года</div>
                                    <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                                </div>
                            </div>
                            <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="#" class="work">
                            <div class="work__header">
                                <div class="work__image" style="background-image: url('images/work__04.jpg');"></div>
                                <div class="work__info">
                                    <div class="work__date">Февраль 2020 года</div>
                                    <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                                </div>
                            </div>
                            <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                        </a>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <div class="completed__desktop">
            <div class="completed__row">

                <div class="completed__col">
                    <a href="#" class="work">
                        <div class="work__header">
                            <div class="work__image" style="background-image: url('images/work__01.jpg');"></div>
                            <div class="work__info">
                                <div class="work__date">Февраль 2020 года</div>
                                <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                            </div>
                        </div>
                        <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                    </a>
                </div>

                <div class="completed__col">
                    <a href="#" class="work">
                        <div class="work__header">
                            <div class="work__image" style="background-image: url('images/work__02.jpg');"></div>
                            <div class="work__info">
                                <div class="work__date">Февраль 2020 года</div>
                                <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                            </div>
                        </div>
                        <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                    </a>
                </div>

                <div class="completed__col">
                    <a href="#" class="work">
                        <div class="work__header">
                            <div class="work__image" style="background-image: url('images/work__03.jpg');"></div>
                            <div class="work__info">
                                <div class="work__date">Февраль 2020 года</div>
                                <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                            </div>
                        </div>
                        <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                    </a>
                </div>

                <div class="completed__col">
                    <a href="#" class="work">
                        <div class="work__header">
                            <div class="work__image" style="background-image: url('images/work__04.jpg');"></div>
                            <div class="work__info">
                                <div class="work__date">Февраль 2020 года</div>
                                <div class="work__title">Комплекс ДГУ 3600 кВт (3 шт. по 1200 кВт) для крупнейшей в России компании обмена интернет-трафиком ММТС-9</div>
                            </div>
                        </div>
                        <div class="work__content">В феврале 2019 года компания «Техэкспо» обеспечила резервным электроснабжением АО «Московская междугородная телефонная станция №9» (АО «ММТС-9»). Это крупнейшая точка межоператорского обмена (пиринг) интернет-трафика в России. Здание АО «ММТС-9» оснащено системами бесперебойного.</div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section>