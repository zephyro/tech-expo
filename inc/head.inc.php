<head  itemscope itemtype="http://schema.org/WPHeader">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title itemprop="headline">Title</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="css/template_styles.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/update.css">


    <meta itemprop="description" name="description" content="описание_страницы">
    <meta itemprop="keywords" name="keywords" content="ключевые_слова_для_страницы">

    <meta property="og:type" content="article">
    <meta property="og:title" content="title">
    <meta property="og:url" content="site_url">
    <meta property="og:description" name="description" content="description">
    <meta property="og:image" content="image.jpg">
</head>